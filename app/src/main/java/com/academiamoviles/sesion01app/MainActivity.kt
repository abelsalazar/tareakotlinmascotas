package com.academiamoviles.sesion01app

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.ayuda.*
import kotlinx.android.synthetic.main.ayuda.view.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvAyuda.setOnClickListener {

            val bottomSheet  = BottomSheetDialog(this,R.style.BottomSheetDialogTheme)
            //Inflar la vista
            val view = LayoutInflater.from(this).inflate(R.layout.ayuda,contenedor)

            view.tvTelefono.text = "Hola"

            bottomSheet.setContentView(view)
            bottomSheet.show()
        }

        btnEnviar.setOnClickListener {

            //1. Validaciones
            var nombres = edtNombres.text.toString()  //Juan Jose
            var edad = edtEdad.text.toString() //30


            //1.1 Nombres no este vacios
            if (nombres.isEmpty()){

                this.mensaje2("Debe ingresar el nombre")
                return@setOnClickListener
            }

            //1.2 Edad no este vacios
            if(edad.isEmpty()){

                this.mensaje2("Debe ingresar la edad")
                return@setOnClickListener
            }

            //1.3 Vacunas de las mascotas
            val result = StringBuilder()
            var seleccionVacuna: CheckBox

            if (!chkLeptospirosis.isChecked && !chkDistemper.isChecked && !chkHepatitis.isChecked && !chkRabia.isChecked && !chkParnovirus.isChecked ){
                this.mensaje2("Debe seleccionar al menos una Vacuna")
                return@setOnClickListener
            }else{
                if (chkLeptospirosis.isChecked){
                    seleccionVacuna = findViewById(R.id.chkLeptospirosis)
                    result.append ("\n"+seleccionVacuna.text.toString())
                }
                if (chkDistemper.isChecked){
                    seleccionVacuna = findViewById(R.id.chkDistemper)
                    result.append ("\n"+chkDistemper.text.toString())
                }
                if (chkHepatitis.isChecked){
                    seleccionVacuna = findViewById(R.id.chkHepatitis)
                    result.append ("\n"+chkHepatitis.text.toString())
                }
                if (chkRabia.isChecked){
                    seleccionVacuna = findViewById(R.id.chkRabia)
                    result.append ("\n"+chkRabia.text.toString())
                }
                if (chkParnovirus.isChecked){
                    seleccionVacuna = findViewById(R.id.chkParnovirus)
                    result.append ("\n"+ chkParnovirus.text.toString())
                }
            }
        //1.4 Seleccion de la Mascota
            var id: Int = radioGroup.checkedRadioButtonId
            val radio: RadioButton = findViewById(id)

        //1.6 llamada a Actividad Destino
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombres",nombres)
                putString("key_edad",edad)
                putString("key_mascota", radio.text.toString())
                putString("key_vacunas", result.toString())
            }

            val intent = Intent(this, DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }
    }
    fun Context.mensaje2(mensaje:String) {

        Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show()
    }
}