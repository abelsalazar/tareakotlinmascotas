package com.academiamoviles.sesion01app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_destino.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        //Obtenemos el bundle
        val bundle : Bundle? = intent.extras

        //let
        bundle?.let { bundleLibriDeNull ->

            val nombres =  bundleLibriDeNull.getString("key_nombres","Desconocido")
            val edad = bundleLibriDeNull.getString("key_edad","0")
            val mascota = bundleLibriDeNull.getString("key_mascota","")
            val vacunas = bundleLibriDeNull.getString("key_vacunas","")


            tvNombresDestino.text = "NOMBRES: $nombres"
            tvEdadDestino.text = "EDAD: $edad"
            tvMascotaDestino.text = "MASCOTA: $mascota"
            tvMascotaVacunas.text = "VACUNAS:\n: $vacunas"

            when {
                mascota=="Perro" -> {
                    imMascota.setImageResource(R.drawable.perro);
                }
                mascota=="Gato" -> {
                    imMascota.setImageResource(R.drawable.gato);
                }
                else -> {
                    imMascota.setImageResource(R.drawable.conejo);
                }
            }
        }

    }
}